$(document).ready(function () {
  if ($(this).scrollTop() > 1) {
    $(".page__header").addClass("fixed");
  } else {
    $(".page__header").removeClass("fixed");
  }
  $(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
      $(".page__header").addClass("fixed");
    } else {
      $(".page__header").removeClass("fixed");
    }
  });

  // красивый select
  if ($(".js-select").length) {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
      // closeOnSelect: false,
    })
  }

  $(".input-file input[type=file]").on("change", function () {
    let file = this.files[0];
    $(this).closest(".input-file").find(".input-file-text").html(file.name);
  });

  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  $("input").on("change", function () {
    if ($(this).val().length) {
      $(this).addClass("not-empty");
    } else {
      $(this).removeClass("not-empty");
    }
  });
  // /маска для инпупов
  $(window).resize(function () {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
    });
  });
  // /красивый select
  $(".js-adaptive-menu-opener").on("click", function () {
    $(this).toggleClass("open");
    $(".js-header-menu-content").slideToggle();
    $(".header").toggleClass("open-menu");
    $("body").toggleClass("fixed");
  });
  $(".js-menu__arr").on("click", function () {
    $(this).parents(".menu__item").toggleClass("active");
    $(this).parents(".menu__item").find(".menu__submenu").slideToggle();
  });
  $(".js-menu__subarr").on("click", function () {
    $(this).parents(".menu__sub-item").toggleClass("active");
    $(this).parents(".menu__sub-item").find("ul").slideToggle();
  });

  $('.js-header-search-opener').on("click", function () {
    $('.js-header-search-form').addClass('open')
    $('.header-search__input').focus()
    $('.header').addClass('has-search')
  })
  $('.js-header-search-cross').on("click", function () {
    $('.js-header-search-form').removeClass('open')
    $('.header').removeClass('has-search')
    $('.header-search__input').val('')
  })
  // слайдер на главной
  if ($(".js-main-slider").length) {
    var mainSwiper = new Swiper(".js-main-slider", {
      navigation: {
        nextEl: ".js-main-slider-next",
        prevEl: ".js-main-slider-prev",
      },
      pagination: {
        el: ".js-main-slider-pagination",
        clickable: true,
      },
    });
    $(".js-main-slider-counter").html(
      mainSwiper.activeIndex + 1 + "/" + mainSwiper.slides.length
    );

    mainSwiper.on("slideChange", function (e) {
      $(".js-main-slider-counter").html(
        this.activeIndex + 1 + "/" + this.slides.length
      );
    });
  }
  // наши проекты
  if ($(".js-project-slider").length) {
    var projectSwiper = new Swiper(".js-project-slider", {
      navigation: {
        nextEl: ".js-project-next",
        prevEl: ".js-project-prev",
      },
      pagination: {
        el: ".js-project-pagination",
        clickable: true,
      },
    });
    $(".js-project-counter").html(
      projectSwiper.activeIndex + 1 + "/" + projectSwiper.slides.length
    );

    projectSwiper.on("slideChange", function (e) {
      $(".js-project-counter").html(
        this.activeIndex + 1 + "/" + this.slides.length
      );
    });
  }
  // СОБЫТИЯ И НОВОСТИ
  let text = $(".js-news-main-tab.active").text();
  $(".js-news-main-active-tab").text(text);

  $(".js-news-main-tab").on("click", function (e) {
    e.preventDefault();
    let text = $(this).text();
    $(".js-news-main-active-tab").text(text);
    $(".js-news-main-active-tab").removeClass("active");
    $(".js-news-main-tabs.active").removeClass("active").slideUp();

    $(".js-news-main-tab").removeClass("active");
    $(this).addClass("active");
    let href = $(this).attr("href");
    $(".js-news-main__content.active").fadeOut();
    $(href).fadeIn().addClass("active");
  });
  $(".js-news-main-active-tab").on("click", function (e) {
    $(this).toggleClass("active");
    $(".js-news-main-tabs").toggleClass("active").slideToggle();
  });
  // карта
  if ($("#map").length) {
    ymaps.ready(function () {
      var myMap = new ymaps.Map(
          "map",
          {
            center: [55.8061, 37.590794],
            zoom: 16,
            controls: ["zoomControl"],
          },
          {}
        ),
        myPlacemark = new ymaps.Placemark(
          myMap.getCenter(),
          {},
          {
            iconLayout: "default#image",
            iconImageHref: "/local/templates/osnova/img/icons/map.svg",
            iconImageSize: [137, 57],
            iconImageOffset: [-64, -57],
          }
        );
      myMap.geoObjects.add(myPlacemark);
      myMap.behaviors.disable("scrollZoom");
      myMap.behaviors.disable("drag");
      myMap.behaviors.disable("multiTouch");
    });
  }

  $(".js-route-name").on("click", function () {
    if (!$(this).parents(".js-route-item").hasClass("active")) {
      $(".js-route-item.active").find(".js-route-content").slideUp();
      $(".js-route-item.active").find(".route__name").removeClass("active");
      $(".js-route-item.active").removeClass("active");
    }

    $(this).parents(".js-route-item").find(".js-route-content").slideToggle();
    $(this)
      .parents(".js-route-item")
      .find(".route__name")
      .toggleClass("active");
    $(this).parents(".js-route-item").toggleClass("active");
  });

  if ($(".js-route-map").length) {
    function init(el, coords, type) {
      var multiRoute = new ymaps.multiRouter.MultiRoute(
        {
          referencePoints: [coords, [55.8061, 37.590794]],
          params: {
            results: 1,
            routingMode: type,
          },
        },
        {
          boundsAutoApply: false,
          routeActiveStrokeColor: "#20E864",
        }
      );
      var Map = new ymaps.Map(
        el,
        {
          center: [55.8061, 37.590794],
          zoom: 14,
          controls: ["zoomControl"],
        },
        {
          buttonMaxWidth: 300,
        }
      );
      Map.geoObjects.add(multiRoute);
      Map.behaviors.disable("scrollZoom");
      Map.behaviors.disable("drag");
      Map.behaviors.disable("multiTouch");
    }

    ymaps.ready(function () {
      $(".js-route-map").each(function () {
        let coords = $(this).attr("data-coords").split(",").map(Number);
        let type = $(this).attr("data-route-type") || "auto";
        let el = $(this).attr("id");
        init(el, coords, type);
      });
    });
  }
  // Наши клиенты
  if ($(".js-clients").length) {
    var mainSwiper = new Swiper(".js-clients", {
      slidesPerView: 5,
      loop: true,
      navigation: {
        nextEl: ".js-clients-next",
        prevEl: ".js-clients-prev",
      },
      pagination: {
        el: ".js-clients-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
        },
        768: {
          slidesPerView: 4,
        },
        1024: {
          slidesPerView: 5,
        },
      },
    });
    // $(".js-clients-counter").html(
    //   mainSwiper.activeIndex + 1 + "/" + mainSwiper.slides.length
    // );

    // mainSwiper.on("slideChange", function (e) {
    //   $(".js-clients-counter").html(
    //     this.activeIndex + 1 + "/" + this.slides.length
    //   );
    // });
  }

  $(".js-cookies-btn").on("click", function (e) {
    e.preventDefault();
    $.cookie("cookies", "true", { expires: 2147483647, path: "/" });
    $(".js-cookies").hide();
  });

  if (!$.cookie("cookies")) {
    $(".js-cookies").show();
  }

  if ($(window).width() < 1200) {
    $(".js-servises-item").on("click", function () {
      $(this).toggleClass("open");
      $(this).find(".js-servises-text").slideToggle();
    });
  }

  if ($(window).width() < 600) {
    $(".benefits__item").on("click", function () {
      $(this).toggleClass("open");
      $(this).find(".benefits__text").slideToggle();
    });
  }

  if ($(window).width() < 600) {
    $(".project__block").on("click", function () {
      $(this).toggleClass("open");
      $(this).find(".project__info").slideToggle();
    });
  }

  if ($(window).width() < 600) {
    $(".footer-menu__link").on("click", function (e) {
      e.preventDefault();
      $(this).toggleClass("open");
      $(this).next(".footer-menu__sub").slideToggle();
    });
  }

  /*----  ----*
	$(".header__menu-btn").on("click", function (e) {
		e.preventDefault();
		$(".js-adaptive-menu-opener").removeClass("open");
		$(".js-header-menu-content").slideUp();
		$(".header").removeClass("open-menu");
		$("body").removeClass("fixed");
		$("html, body").animate(
			{
				scrollTop: $(".contact-us").offset().top - 220 + "px",
			},
			{
				duration: 500,
				easing: "swing",
			}
		);
	});
	/*----  ----*/

  $('a[href="#callback-form"], .servises__name').on("click", function (e) {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $(".contact-us").offset().top - 220 + "px",
      },
      {
        duration: 500,
        easing: "swing",
      }
    );
  });
  $(".footer-menu a[href='#']")
    .addClass("no-hov")
    .on("click", function (e) {
      e.preventDefault();
    });

  let scrollTop = $(window).scrollTop();
  $(window).scroll(function () {
    $(".marquee__text").css("animation-play-state", "running");
    var scrollSpeed = 10 + Math.abs($(window).scrollTop() - scrollTop) * 0.1; // скорость прокрутки зависит от скорости скролла
    //$('.marquee__text').css('animation-duration', scrollSpeed + 's');

    scrollTop = $(window).scrollTop();
  });

  $(window).scroll(function () {
    clearTimeout($.data(this, "scrollTimer"));
    $.data(
      this,
      "scrollTimer",
      setTimeout(function () {
        $(".marquee__text").css("animation-play-state", "paused");
      }, 250)
    );
  });
  $(window).scroll(function () {
    let scrollTop = $(window).scrollTop();
    let footerPosition = $(".footer").offset().top;

    if (scrollTop > $(window).height() * 1.5) {
      $(".js-top-btn").fadeIn();
    } else {
      $(".js-top-btn").fadeOut();
    }

    if (scrollTop > footerPosition - $(window).height() - 78) {
      $(".js-top-btn").addClass("not-fixed");
    } else {
      $(".js-top-btn").removeClass("not-fixed");
    }
  });

  $(".js-top-btn").on("click", function () {
    $("body,html").animate(
      {
        scrollTop: 0,
      },
      1000,
      "linear"
    );
  });

  //  отраслевая экспертиза
  if ($(".js-slider-industry-swiper").length) {
    var swiper = new Swiper(".js-slider-industry-swiper", {
      effect: "fade",
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      on: {
        slideChange: function () {
          var activeIndex = this.activeIndex;
          $(".js-slider-industry-item").removeClass("active");
          $(
            '.js-slider-industry-item[data-slide="' + activeIndex + '"]'
          ).addClass("active");
        },
      },
    });

    $('.js-slider-industry-item[data-slide="0"]').addClass("active");

    if ($(window).width() > 1200) {
      swiper.autoplay.stop();
    }

    $(".js-slider-industry-item").on("mouseover", function () {
      if ($(window).width() > 1200) {
        var slideIndex = $(this).data("slide");
        swiper.slideTo(slideIndex);
      }
    });

    $(window).on("resize", function () {
      if ($(window).width() > 1200) {
        swiper.autoplay.stop();
      } else {
        swiper.autoplay.start();
      }
    });
  }
  // / отраслевая экспертиза

  $(".js-unfolding-title").on("click", function () {
    if (!$(this).parents(".js-unfolding-item").hasClass("active")) {
      $(".js-unfolding-item.active").find(".js-unfolding-content").slideUp();
      $(".js-unfolding-item.active")
        .find(".js-unfolding-title")
        .removeClass("active");
      $(".js-unfolding-item.active").removeClass("active");
    }

    $(this)
      .parents(".js-unfolding-item")
      .find(".js-unfolding-content")
      .slideToggle();
    $(this)
      .parents(".js-unfolding-item")
      .find(".js-unfolding-title")
      .toggleClass("active");
    $(this).parents(".js-unfolding-item").toggleClass("active");
  });
  if ($(".js-unfolding-item").length) {
    $(".js-unfolding-item:first-child")
      .find(".js-unfolding-content")
      .slideToggle();
    $(".js-unfolding-item:first-child")
      .find(".js-unfolding-title")
      .toggleClass("active");
    $(".js-unfolding-item:first-child").toggleClass("active");
  }

  if ($(".js-faq-item").length) {
    function activeFaq(item, first) {
      if ($(window).width() > 1200) {
        if (!item.find(".js-faq-question").hasClass("active") && !first) {
          $(".js-faq-question.active").removeClass("active");
          $(".js-faq-item.active").removeClass("active");
        }
        item.addClass("active");
        item.find(".js-faq-question").addClass("active");
        let text = item.find(".js-faq-answer").html();
        item.parents(".faq").find(".js-content").html(text);
      } else {
        if (!item.find(".js-faq-question").hasClass("active")) {
          $(".js-faq-question.active")
            .parents(".js-faq-item")
            .find(".js-faq-answer")
            .slideUp();
          $(".js-faq-question.active").removeClass("active");
        }
        item.toggleClass("active");
        item.find(".js-faq-question").toggleClass("active");
        item.find(".js-faq-answer").toggleClass("active").slideToggle();
      }
    }
    $(".js-faq-item").on("click", function () {
      activeFaq($(this), false);
    });
    $(".js-faq-item:first-child").each(function () {
      activeFaq($(this), true);
    });
  }
  let programSwiper = null;

  function makeProgramsSwiper(swiper) {
    if (swiper.hasClass("programs--one")) {
      return;
    }
    let slidesCount = 4;
    let slidesCountSmall = 3;
    if ($(".news-detail").length) {
      slidesCount = 2;
      slidesCountSmall = 2;
    }
    if ($(".service-programs").length) {
      slidesCount = 3;
      slidesCountSmall = 3;
    }
    programSwiper = new Swiper(swiper, {
      slidesPerView: slidesCount,
      wrapperClass: "programs__wrap",
      slideClass: "programs__item",
      navigation: {
        nextEl: ".js-programs-next",
        prevEl: ".js-programs-prev",
      },
      pagination: {
        el: ".js-programs-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 1,
        },
        768: {
          slidesPerView: 2,
        },
        1100: {
          slidesPerView: 2,
        },
        1600: {
          slidesPerView: slidesCountSmall,
        },
        1200: {
          slidesPerView: slidesCount,
        },
      },
    });

    $(".js-programs-counter").html(
      programSwiper.activeIndex + 1 + "/" + programSwiper.slides.length
    );

    programSwiper.on("slideChange", function () {
      $(".js-programs-counter").html(
        this.activeIndex + 1 + "/" + this.slides.length
      );
    });
  }

  if (
    $(".js-programs").length &&
    !$(".js-programs").hasClass("js-programs-swiper")
  ) {
    if ($(window).width() < 600) {
      makeProgramsSwiper($(".js-programs"));
    }

    $(window).on("resize", function () {
      if ($(window).width() < 600) {
        makeProgramsSwiper($(".js-programs"));
      } else if (programSwiper) {
        programSwiper.destroy();
        programSwiper = null;
      }
    });
  }

  if ($(".js-programs-swiper").length) {
    makeProgramsSwiper($(".js-programs-swiper"));
  }

  $(".js-programs-mobile-title").on("click", function () {
    $(this).slideUp();
    $(".js-programs-mobile-content").slideDown();
  });

  $(".js-programs-mobile-hide").on("click", function () {
    $(".js-programs-mobile-title").slideDown();
    $(".js-programs-mobile-content").slideUp();
  });

  if ($(".scrollable-list").length) {
    let text = "";
    let i = 0;
    $(".js-scrollable-list-item").each(function () {
      i++;
      let textPart = $(this).find(".js-scrollable-list-answer").html();
      text =
        text +
        "<div id='" +
        i +
        "' class='js-scrollable-block'> " +
        textPart +
        "</div>";
    });
    $(".js-scrollable-list-content").html(text);

    if ($(window).width() > 1200) {
      var lastScrollTop = 0;
      $(window).scroll(function () {
        var scrollPos = $(window).scrollTop();
        var rightBlockPosTop = $(".js-scrollable-list-content").offset().top;
        var rightBlockPosBottom =
          rightBlockPosTop + $(".js-scrollable-list-content").outerHeight();

        if (
          scrollPos + 100 >= rightBlockPosTop &&
          scrollPos + $(".js-scrollable-list-fixed").outerHeight() + 100 <=
            rightBlockPosBottom
        ) {
          $(".js-scrollable-list-fixed").css("position", "fixed");
        } else {
          $(".js-scrollable-list-fixed").css("position", "static");

          if (
            (lastScrollTop > scrollPos &&
              scrollPos - $(window).height() >= rightBlockPosTop) ||
            (lastScrollTop < scrollPos &&
              scrollPos + $(window).height() >
                rightBlockPosBottom - $(".js-scrollable-block").outerHeight())
          ) {
            $(".scrollable-list").css("align-items", "end");
          } else {
            $(".scrollable-list").css("align-items", "start");
          }
        }
        lastScrollTop = scrollPos;

        $(".js-scrollable-block").each(function () {
          var BlockPosTop = $(this).offset().top;
          var BlockPosBottom = BlockPosTop + $(this).height();
          let BlockId = $(this).attr("id");

          if (scrollPos + 200 >= BlockPosTop && scrollPos <= BlockPosBottom) {
            $(".js-scrollable-list-item.active").removeClass("active");
            $(".js-scrollable-list-item:nth-child(" + BlockId + ")").addClass(
              "active"
            );
          } else {
            $(
              ".js-scrollable-list-item:nth-child(" + BlockId + ")"
            ).removeClass("active");
          }
        });
      });
    }

    function activeScrollable(item) {
      if ($(window).width() > 1200) {
        var index = $(".js-scrollable-list-item").index(item) + 1;

        let top = $("#" + index).offset().top - 100;

        $("body,html").animate(
          {
            scrollTop: top,
          },
          100,
          "linear"
        );
      } else {
        if (!item.find(".js-scrollable-list-question").hasClass("active")) {
          $(".js-scrollable-list-question.active")
            .parents(".js-scrollable-list-item")
            .find(".js-scrollable-list-answer")
            .slideUp();

          $(".js-scrollable-list-question.active").removeClass("active");
        }
        item.find(".js-scrollable-list-question").toggleClass("active");
        item
          .find(".js-scrollable-list-answer")
          .toggleClass("active")
          .slideToggle();
      }
    }

    $(".js-scrollable-list-item").on("click", function () {
      activeScrollable($(this));
    });
  }

  $(".js-questions-ask").on("click", function () {
    if (!$(this).parents(".js-questions-item").hasClass("active")) {
      $(".js-questions-item.active").find(".js-questions-answer").slideUp();
      $(".js-questions-item.active")
        .find(".js-questions-ask")
        .removeClass("active");
      $(".js-questions-item.active").removeClass("active");
    }

    $(this)
      .parents(".js-questions-item")
      .find(".js-questions-answer")
      .slideToggle();
    $(this)
      .parents(".js-questions-item")
      .find(".js-questions-ask")
      .toggleClass("active");
    $(this).parents(".js-questions-item").toggleClass("active");
  });

  if ($(".js-scroll-block").length) {
    if ($(window).width() > 1200) {
      var lastScrollTop = 0;
      $(window).scroll(function () {
        var scrollPos = $(window).scrollTop();

        $(".js-scroll-block").each(function () {
          var BlockPosTop = $(this).offset().top;
          var BlockPosBottom = BlockPosTop + $(this).height();
          let BlockId = $(this).attr("id");

          if (scrollPos + 200 >= BlockPosTop && scrollPos <= BlockPosBottom) {
            $(".js-scroll-list-item.active").removeClass("active");
            $(".js-scroll-list-item[href='#" + BlockId + "']").addClass(
              "active"
            );
          } else {
            $(".js-scroll-list-item[href='#" + BlockId + "']").removeClass(
              "active"
            );
          }
        });
      });
    }
  }

  // слайдер новостей
  if ($(".js-news-slider").length) {
    var newsSwiper = new Swiper(".js-news-slider", {
      slidesPerView: 3,
      navigation: {
        nextEl: ".js-news-next",
        prevEl: ".js-news-prev",
      },
      pagination: {
        el: ".js-news-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 1,
        },
        768: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: 3,
        },
      },
    });
    $(".js-news-counter").html(
      newsSwiper.activeIndex + 1 + "/" + newsSwiper.slides.length
    );

    newsSwiper.on("slideChange", function (e) {
      $(".js-news-counter").html(
        this.activeIndex + 1 + "/" + this.slides.length
      );
    });
  }
  if ($(".js-serv-slider").length) {
    let slidesCount = 3;
    if ($(".js-serv-slider").hasClass('js-serv-slider--2')) {
      slidesCount = 2;
    }
    var servSwiper = new Swiper(".js-serv-slider", {
      slidesPerView: slidesCount,
      navigation: {
        nextEl: ".js-serv-next",
        prevEl: ".js-serv-prev",
      },
      pagination: {
        el: ".js-serv-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 1,
        },
        768: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: slidesCount,
        },
      },
    });
    $(".js-serv-counter").html(
      servSwiper.activeIndex + 1 + "/" + servSwiper.slides.length
    );
  
    servSwiper.on("slideChange", function (e) {
      $(".js-serv-counter").html(
        this.activeIndex + 1 + "/" + this.slides.length
      );
    });
  }
  // Результаты поиска

  if ($(".js-search-results-content").length) {
    $(".js-search-results-content").each(function () {
      let items = $(this).find(".search-results__item");
      if (items.length > 3) {
        let sumHeight = 0;
        items.slice(0, 3).each(function () {
          sumHeight += $(this).outerHeight();
        });
        $(this)
          .find(".js-search-results-list")
          .css("max-height", sumHeight - 2);
        $(this).find(".js-search-results-show").show();
      }
    });
    $(".js-search-results-show").on("click", function () {
      $(this).hide();
      $(this)
        .parents(".js-search-results-content")
        .find(".js-search-results-hide")
        .show();
      $(this)
        .parents(".js-search-results-content")
        .find(".js-search-results-list")
        .css("max-height", "5000px");
    });
    $(".js-search-results-hide").on("click", function () {
      let beforeScrollTop = $(window).scrollTop();
      let blockHeight = $(this)
        .parents(".js-search-results-content")
        .find(".js-search-results-list")
        .outerHeight();

      $(this).hide();
      $(this)
        .parents(".js-search-results-content")
        .find(".js-search-results-show")
        .show();
      $(this)
        .parents(".js-search-results-content")
        .find(".js-search-results-list")
        .css("max-height", "5000px");
      let items = $(this)
        .parents(".js-search-results-content")
        .find(".search-results__item");
      let sumHeight = 0;
      if (items.length > 3) {
        items.slice(0, 3).each(function () {
          sumHeight += $(this).outerHeight();
        });
        $(this)
          .parents(".js-search-results-content")
          .find(".js-search-results-list")
          .css("max-height", sumHeight - 2);
      }

      let scrollOffset = blockHeight - sumHeight;

      $("html, body").scrollTop(beforeScrollTop - scrollOffset);
    });
  }
  $(".js-search-form-clear").on("click", function () {
    $(".js-search-form-input").val("");
  });

  $(document).on("click", ".js-work-form-block-tab", function (e) {
    e.preventDefault();
    $(".js-work-form-block-tab.active").removeClass("active");
    $(this).addClass("active");
    let href = $(this).attr("href");
    $(".js-work-form-block-content.active").removeClass("active").hide();
    $(href).show().addClass("active");
  });

  $(".js-tab").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
    if ($(this).hasClass("js-tab--all")) {
      if ($(this).hasClass("active")) {
        $(".js-tab:not(.js-tab--all)").each(function () {
          $(this).removeClass('active')
          let href = $(this).attr("href");
            $(href).show();
        });
      } 
    } else {
      $(".js-tab").each(function () {
        $(".js-tab--all").removeClass('active')
        let href = $(this).attr("href");
        if ($(this).hasClass("active")) {
          $(href).show();
        } else {
          $(href).hide();
        }
      });
    }
  });
$('.js-tab-select').on("change", function (e) {
  if ($(this).val() == 'all') {
    $(".js-tab:not(.js-tab--all)").each(function () {
      $(this).removeClass('active')
      let href = $(this).attr("href");
        $(href).show();
    });
  } else {
    let activeHref = '#' + $(this).val();
    $(".js-tab").each(function () {
      let href = $(this).attr("href");
      if (href == activeHref) {
        $(href).show();
      } else {
        $(href).hide();
      }
    });
  }
})

if($('h1').text() == "Поиск") {
  $('.title--top').addClass('title--noborder')
}

});
